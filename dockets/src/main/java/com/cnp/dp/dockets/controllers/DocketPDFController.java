package com.cnp.dp.dockets.controllers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cnp.dp.dockets.models.Greeting;
import com.cnp.dp.dockets.processors.BatchDockets;

@RestController
public class DocketPDFController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping("/pdf")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	@RequestMapping("/genpdf")
	public String genpdf(@RequestParam(value = "name", defaultValue = "World123") String name) {
		BatchDockets batchDockets = new BatchDockets();
		batchDockets.demo();
		return "done";
	}

//	@RequestMapping(value="/getpdf", method=RequestMethod.POST)
//	public ResponseEntity<byte[]> getPDF(@RequestBody String json) {
//	    // convert JSON to Employee 
//	    Employee emp = convertSomehow(json);
//
//	    // generate the file
//	    PdfUtil.showHelp(emp);
//
//	    // retrieve contents of "C:/tmp/report.pdf" that were written in showHelp
//	    byte[] contents = (...);
//
//	    HttpHeaders headers = new HttpHeaders();
//	    headers.setContentType(MediaType.parseMediaType("application/pdf"));
//	    String filename = "output.pdf";
//	    headers.setContentDispositionFormData(filename, filename);
//	    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//	    ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
//	    return response;
//	}
//	

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public ResponseEntity<Resource> download() throws IOException {
		String filePath = "c:/tmp/cnp/finalpacket.pdf";		
		InputStream inputStream = new FileInputStream(new File(filePath));
		InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		String filename = "SignedDockets.pdf";
		headers.setContentDispositionFormData(filename, filename);
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.setContentLength(Files.size(Paths.get(filePath)));
		return new ResponseEntity(inputStreamResource, headers, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/gendownload", method = RequestMethod.GET)
	public ResponseEntity<Resource> generatePdfnDownload() throws IOException {
		BatchDockets batchDockets = new BatchDockets();
		ByteArrayOutputStream baos = batchDockets.demo2();
		String filePath = "c:/tmp/cnp/finalpacket.pdf";		
		InputStream inputStream = new ByteArrayInputStream(baos.toByteArray());
		InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		String filename = "SignedDockets.pdf";
		headers.setContentDispositionFormData(filename, filename);
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.setContentLength(baos.size());
		return new ResponseEntity(inputStreamResource, headers, HttpStatus.OK);
	}
}