package com.cnp.dp.dockets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocketsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocketsApplication.class, args);
	}
}
