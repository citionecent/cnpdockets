package com.cnp.dp.dockets.processors;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;

public class BatchDockets {

	// get pdf
	// save the 4th page to file
	// get the 4th page
	// add signature
	// save to 4th page signed
	// get the original 3rd page
	// get the signed 4th page
	// create a new pdf with the sign docket
	// repeat
	
	static String source = "c:/tmp/cnp/Docket111.pdf";
	static String target = "c:/tmp/cnp/Docket111signed4thpage.pdf";
	static String target4thpage = "c:/tmp/cnp/Docket1114thpage.pdf";
	static String targetSigned = "c:/tmp/cnp/Docket111Signed.pdf";
	//static String imageSource = "c:/tmp/cnp/signature.jpg";	  	   	
	static String imageSource = "c:/tmp/cnp/steven.png";
	static String dest = "c:/tmp/cnp/finalpacket.pdf";	  	  
	
	public void demo() {
		System.out.println( "Hello World!" );
        BatchDockets bd = new BatchDockets();
        try {        	       
        	ByteArrayOutputStream[] dockets = new ByteArrayOutputStream[3];
        	for(int i = 0; i<3; i++) {
        		PdfDocument docket = bd.getPdfbyPath(source);
            	ByteArrayOutputStream page4Unsigned = bd.save4thPageToMemory(docket);
            	System.out.println(page4Unsigned.size());
            	ByteArrayOutputStream page4Signed = bd.addSignatureToPageInMemory(page4Unsigned, imageSource);
            	System.out.println(page4Signed.size());
            	ByteArrayOutputStream signedDocketbaos = bd.save3rdAndSigned4thInMemory(docket, page4Signed);
            	System.out.println(signedDocketbaos.size());
            	dockets[i] = signedDocketbaos;
            	docket.close();
        	}
        	
        	bd.mergeAllDockets(dockets, dest);
        	
        	//bd.save4thPageToFile(target4thpage, bd.getPdfbyPath(source));
			//bd.addSignatureToPage(target4thpage, target, imageSource);
			//bd.save3rdAndSigned4th(targetSigned, bd.getPdfbyPath(source), bd.getPdfbyPath(target));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println( "Done!" );
	}
	
	public ByteArrayOutputStream demo2() {
		System.out.println( "Hello World!" );
		ByteArrayOutputStream baosReturn = new ByteArrayOutputStream();
        BatchDockets bd = new BatchDockets();
        
        File folder = new File("c:/tmp/dockets");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
          if (listOfFiles[i].isFile()) {
            System.out.println("File " + listOfFiles[i].getName());
          } else if (listOfFiles[i].isDirectory()) {
            System.out.println("Directory " + listOfFiles[i].getName());
          }
        }
        
        
        try {        	       
        	ByteArrayOutputStream[] dockets = new ByteArrayOutputStream[listOfFiles.length];
        	for(int i = 0; i<listOfFiles.length; i++) {
        		System.out.println("process document " + i);
        		PdfDocument docket = bd.getPdfbyPath(listOfFiles[i].getAbsolutePath());
            	ByteArrayOutputStream page4Unsigned = bd.save4thPageToMemory(docket);
            	System.out.println(page4Unsigned.size());
            	ByteArrayOutputStream page4Signed = bd.addSignatureToPageInMemory(page4Unsigned, imageSource);
            	System.out.println(page4Signed.size());
            	ByteArrayOutputStream signedDocketbaos = bd.save3rdAndSigned4thInMemory(docket, page4Signed);
            	System.out.println(signedDocketbaos.size());
            	dockets[i] = signedDocketbaos;
            	docket.close();
        	}
        	System.out.println("done processing pdf docket files ");
        	baosReturn = bd.mergeAllDocketsInMemory(dockets);
        	
        	//bd.save4thPageToFile(target4thpage, bd.getPdfbyPath(source));
			//bd.addSignatureToPage(target4thpage, target, imageSource);
			//bd.save3rdAndSigned4th(targetSigned, bd.getPdfbyPath(source), bd.getPdfbyPath(target));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println( "Done!" );
		return baosReturn;
		
	}
	

	public void save3rdAndSigned4th(String output, PdfDocument origPdf, PdfDocument signedPdf) throws FileNotFoundException {
		PdfDocument pdf = new PdfDocument(new PdfWriter(output));
		pdf.addPage(origPdf.getPage(3).copyTo(pdf));
		pdf.addPage(signedPdf.getPage(1).copyTo(pdf));
		pdf.close();
	}
	
	public ByteArrayOutputStream save3rdAndSigned4thInMemory(PdfDocument origPdf, ByteArrayOutputStream signedPage) throws IOException {
		ByteArrayOutputStream signedDocket = new ByteArrayOutputStream();
		PdfDocument pdf = new PdfDocument(new PdfWriter(signedDocket));
		pdf.addPage(origPdf.getPage(3).copyTo(pdf));
		PdfDocument signed4thPagePdf = new PdfDocument(new PdfReader(new ByteArrayInputStream(signedPage.toByteArray()))); 								
		pdf.addPage(signed4thPagePdf.getPage(1).copyTo(pdf));
		pdf.close();
		signed4thPagePdf.close();
		return signedDocket;
	}
	
	public void save4thPageToFile(String output, PdfDocument origPdf) throws FileNotFoundException {
		PdfDocument pdf = new PdfDocument(new PdfWriter(output));
		pdf.addPage(origPdf.getPage(4).copyTo(pdf));
		pdf.close();
	}
	
	public ByteArrayOutputStream save4thPageToMemory(PdfDocument origPdf) throws FileNotFoundException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		//Document document = new Document(new PdfDocument(new PdfWriter(baos)));
		
		System.out.println(baos.size());
		PdfDocument pdf = new PdfDocument(new PdfWriter(baos));
		pdf.addPage(origPdf.getPage(4).copyTo(pdf));
		pdf.close();		
		
		System.out.println(baos.size());
		return baos;
		
	}

	public PdfDocument getOutputPdfbyPath(String path) throws FileNotFoundException {
		return new PdfDocument(new PdfWriter(path));
	}

	public PdfDocument getPdfbyPath(String path) throws IOException {
		return new PdfDocument(new PdfReader(path));
	}

	public void addSignatureToPage(String source, String target, String imageSource)
			throws FileNotFoundException, IOException {

		// Modify PDF located at "source" and save to "target"
		PdfDocument pdfDocument = new PdfDocument(new PdfReader(source), new PdfWriter(target));
		// Document to add layout elements: paragraphs, images etc
		Document document = new Document(pdfDocument);

		// Load image from disk
		ImageData imageData = ImageDataFactory.create(imageSource);
		// Create layout image object and provide parameters. Page number = 1
		Image image = new Image(imageData).scaleAbsolute(100, 200).setFixedPosition(1, 25, 25);
		// This adds the image to the page
		document.add(image);

		// Don't forget to close the document.
		// When you use Document, you should close it rather than PdfDocument instance
		document.close();
	}

	public ByteArrayOutputStream addSignatureToPageInMemory(ByteArrayOutputStream baos, String imageSource)
			throws FileNotFoundException, IOException {
		ByteArrayOutputStream baosReturn = new ByteArrayOutputStream();
		// Modify PDF located at "source" and save to "target"
		PdfDocument pdfDocument = new PdfDocument(new PdfReader(new ByteArrayInputStream(baos.toByteArray())), new PdfWriter(baosReturn));
		// Document to add layout elements: paragraphs, images etc
		Document document = new Document(pdfDocument);

		// Load image from disk
		ImageData imageData = ImageDataFactory.create(imageSource);
		// Create layout image object and provide parameters. Page number = 1
		Image image = new Image(imageData).scaleAbsolute(200, 25).setFixedPosition(1, 50, 175);
		// This adds the image to the page
		document.add(image);

		// Don't forget to close the document.
		// When you use Document, you should close it rather than PdfDocument instance
		document.close();
		return baosReturn;
	}
	
	public void mergeAllDockets(ByteArrayOutputStream[] docketsbaos, String dest) throws IOException {
		
		System.out.println("merge all outputstream to pdf");
		PdfDocument finalPdf = new PdfDocument(new PdfWriter(dest));		
		for(int i = 0; i<docketsbaos.length; i++) {
			PdfReader reader = new PdfReader(new ByteArrayInputStream(docketsbaos[i].toByteArray()));
			PdfDocument pdfDoc = new PdfDocument(reader);
			pdfDoc.copyPagesTo(1, pdfDoc.getNumberOfPages(), finalPdf);
			pdfDoc.close();			
		}
		finalPdf.close();
		System.out.println("done merging");
	}

	public ByteArrayOutputStream mergeAllDocketsInMemory(ByteArrayOutputStream[] docketsbaos) throws IOException {
		ByteArrayOutputStream baosReturn = new ByteArrayOutputStream();
		System.out.println("merge all outputstream to pdf");
		PdfDocument finalPdf = new PdfDocument(new PdfWriter(baosReturn));		
		for(int i = 0; i<docketsbaos.length; i++) {
			System.out.println("merging doc " + i);
			PdfReader reader = new PdfReader(new ByteArrayInputStream(docketsbaos[i].toByteArray()));
			PdfDocument pdfDoc = new PdfDocument(reader);
			pdfDoc.copyPagesTo(1, pdfDoc.getNumberOfPages(), finalPdf);
			pdfDoc.close();			
		}
		finalPdf.close();
		System.out.println("done merging");
		return baosReturn;
	}
	
}
